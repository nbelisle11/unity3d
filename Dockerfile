FROM ubuntu:18.04

RUN apt-get update -qq; \
  apt-get install -qq -y \
  cpio \
  curl \
  gconf-service \
  git \
  lib32gcc1 \
  lib32stdc++6 \
  libarchive-dev \
  libasound2 \
  libarchive13 \
  libc6 \
  libc6-i386 \
  libcairo2 \
  libcap2 \
  libcups2 \
  libdbus-1-3 \
  libexpat1 \
  libfontconfig1 \
  libfreetype6 \
  libgcc1 \
  libgconf-2-4 \
  libgdk-pixbuf2.0-0 \
  libgl1-mesa-glx \
  libglib2.0-0 \
  libglu1-mesa \
  libgtk2.0-0 \
  libgtk-3-0 \
  libnotify4 \
  libnspr4 \
  libnss3 \
  libpango1.0-0 \
  libstdc++6 \
  libsoup2.4-1 \
  libx11-6 \
  libxcomposite1 \
  libxcursor1 \
  libxdamage1 \
  libxext6 \
  libxfixes3 \
  libxi6 \
  libxrandr2 \
  libxrender1 \
  libxtst6 \
  libunwind-dev \
  zlib1g \
  pulseaudio \
  debconf \
  npm \
  p7zip-full \
  p7zip-rar \
  ruby-dev \
  sudo \
  xdg-utils \
  lsb-release \
  libpq5 \
  xvfb \
  wget \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*; \
  gem install u3d; 

RUN export LC_ALL=en_US.UTF-8; \
  export LANG=en_US.UTF-8; \
  sudo u3d install 2019.1.0f2 -p Unity,Mac-Mono,Windows-Mono --verbose -t;

# Clean up
RUN rm -rf /tmp/* /var/tmp/*
